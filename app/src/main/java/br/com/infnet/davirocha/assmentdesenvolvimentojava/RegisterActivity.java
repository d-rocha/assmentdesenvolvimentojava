package br.com.infnet.davirocha.assmentdesenvolvimentojava;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import Interfaces.CommonActivity;
import Model.User;


public class RegisterActivity extends CommonActivity implements DatabaseReference.CompletionListener, View.OnClickListener {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private User user;
    private AutoCompleteTextView name;
    private AutoCompleteTextView email;
    private AutoCompleteTextView cpf;
    private AutoCompleteTextView password;
    private AutoCompleteTextView confirmPassword;
    private FloatingActionButton FabCadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                if (firebaseUser == null || user.getId() != null) {
                    return;
                }

                user.setId(firebaseUser.getUid());
                user.saveDB(RegisterActivity.this);
            }
        };

        inicializarViews();

        FabCadastrar = (FloatingActionButton) findViewById(R.id.fab_enviarDados);
        FabCadastrar.setOnClickListener(this);

    }

    protected void inicializarViews() {
        name = (AutoCompleteTextView) findViewById(R.id.edt_Nome);
        email = (AutoCompleteTextView) findViewById(R.id.edt_Email);
        cpf = (AutoCompleteTextView) findViewById(R.id.edt_CPF);
        password = (AutoCompleteTextView) findViewById(R.id.edt_Senha);
        confirmPassword = (AutoCompleteTextView) findViewById(R.id.edt_Senha_Confirmar);
        progressBar = (ProgressBar) findViewById(R.id.sign_up_progress);
    }

    protected void inicializarUsuario() {
        user = new User();
        user.setName(name.getText().toString());
        user.setEmail(email.getText().toString());
        user.setCpf(cpf.getText().toString());
        user.setPassword(password.getText().toString());
    }

    @Override
    public void onClick(View v) {
        inicializarUsuario();

        String NOME = name.getText().toString();
        String EMAIL = email.getText().toString();
        String CPF = cpf.getText().toString();
        String SENHA = password.getText().toString();
        String CONFIRMARSENHA = confirmPassword.getText().toString();

        boolean ok = true;

        if (NOME.isEmpty()) {
            name.setError("O campo nome não pode ser vazio");
            ok = false;
        }

        if (EMAIL.isEmpty()) {
            email.setError("O campo email não pode ser vazio");
            ok = false;
        }

        if (CPF.isEmpty()) {
            cpf.setError("Por favor, informe o cpf!");
            ok = false;
        }

        if (SENHA.isEmpty()) {
            password.setError("Por favor, informe uma senha!");
            ok = false;
        }

        if (SENHA.length() < 6){
            Toast.makeText(getApplicationContext(), "Senha muito curta, minimo 6 characteres!", Toast.LENGTH_SHORT).show();
            ok = false;
        }

        if (CONFIRMARSENHA.isEmpty()) {
            confirmPassword.setError("Por favor, repita a senha!");
            ok = false;
        }

        if (ok) {
            FabCadastrar.setEnabled(false);
            progressBar.setFocusable(true);

            openProgressBar();
            saveUser();
        } else {
            closeProgressBar();
        }

        validarSenha();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthStateListener != null) {
            mAuth.removeAuthStateListener(mAuthStateListener);
        }
    }

    private void validarSenha(){
        password = findViewById(R.id.edt_Senha);
        confirmPassword = findViewById(R.id.edt_Senha_Confirmar);

        String nPassword = password.getText().toString();
        String cPassword = confirmPassword.getText().toString();

        if(!nPassword.equals("") && cPassword.equals(nPassword)){
            msgAlert("");
        }else{
            msgAlert("Senhas devem ser iguais");
        }
    }

    private void msgAlert(String string){
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    private void saveUser() {

        mAuth.createUserWithEmailAndPassword(
                user.getEmail(),
                user.getPassword()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (!task.isSuccessful()) {
                    closeProgressBar();
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showSnackbar(e.getMessage());
                FabCadastrar.setEnabled(true);
            }
        });
    }

    @Override
    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
        mAuth.signOut();

        showToast("Usuário criado com sucesso!");
        closeProgressBar();
        finish();
    }

    @Override
    protected void onConnectionFailed(ConnectionResult connectionResult) {

    }
}